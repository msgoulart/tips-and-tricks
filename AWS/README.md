# AWS Tricks

## Mount EFS to Workspace

When using a Linux workspace we can mount customer's EFS directly
in our workspace.

To achieve that:

1. Adjust EFS Security Group:
An `Ingress` rule needs to be added. Protocol `NFS` and the workspace
range `172.28.129.0/24`.

2. Go to EFS and copy the mount command that will look like this:
```
sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport <EFS_PRIVATE_IP>:/ efs
```