# WORKSPACE TRICKS

Here I'm summarizing some tips and tricks to have your workspace running properly.

## Windows workspace


## Linux workspace

For linux workspace you can use RDP, we've installed XRDP.
Before sugin RDP for the very first time, login using SSH and execute the following commands:

```
sudo systemctl stop bdsec
sudo systemctl stop dbsec-daemon
sudo systemctl disable bdsec
sudo systemctl disable bdsec-daemon
```

However, in order to make it work smothy do the following steps:
1. Disable `Use all my monitors for the remote session` in the `Display` tab of the RDP client (Windows).
2. In `Choose color depth` choose `High Color (16 bit)`
3. In `Experience` tab choose `Low-speed broadband`

### Running Docker on Workspace

I've faced issues to configure proxy for the docker daemon.
At this moment I'm running docker version `20`.

#### Note
If you have a newer version try [this](https://docs.docker.com/config/daemon/systemd/#httphttps-proxy)

Follow the steps:
1. Create a systemd drop-in directory:
```
sudo mkdir /etc/systemd/system/docker.service.d
```
	
2. Add proxy in /etc/systemd/system/docker.service.d/http-proxy.conf file:
	
```
sudo vim /etc/systemd/system/docker.service.d/http-proxy.conf
```

And then place the content:

```
[Service]
Environment="HTTP_PROXY=http://192.168.197.56:8080/"
Environment="HTTPS_PROXY=https://192.168.197.56:8080/"
Environment="NO_PROXY=localhost,127.0.0.1,localaddress,.localdomain.com"
```

3. Flush changes:
```
sudo systemctl daemon-reload
```
	
4. Restart Docker:
```
sudo systemctl restart docker
```


